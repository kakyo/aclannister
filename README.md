### How to compile ? ###

* You will need any version of Visual Studio 2013 to open and compile the project.
* In Visual Studio just press shift+F5 to compile and run the project.

### Working with Console ###

* To execute the console, open the command console (windows+r , type "cmd" and press "enter" )
* Navigate to <solution_folder>/ACLannister.Console/bin/Debug
* Then execute: ACLannister.Console.exe <graph_notation> <action> <path> <stop_count>
* At any moment, type "ACLannister.Console.exe help|? <action>" for instructions

### Working with RESTful Service ###

* In VisualStudio, set "ACLannister.Restful" as the startup project (in the project context menu). 
* Press shift+F5 and wait the your default browser to launch
* In the browser, navigate to : <localhost_with_port>/Service.svc/<graph_notation>/<action>/<path>/<stop_count>
* For every action, you should append the desired format (XML or JSON) as a query string. E.g.: ?type=json

### Understanding the parameters ###

* **graph_notation** : it should be a collection of pairs of city names with associated distance connected by a comma. E.g.: AB4, BC13, CD123

    * **NOTE** : in console you can also provide a relative/absolute path to a file containing the graph notation. if the path is relative, put the file in the same folder as *ACLannister.Console.exe*

* **action** : use one of the following "Distance" or "AvailableRoutes" or "ShortestRoute"

* **path** : It should be a pair of cities name connected by a hypen. E.g.: A-B ; 

    * **NOTE** :  in case of "Distance" action, it can be more than 2 cities. E.g.: A-B-C-D

* **stop_count** : this option is only required for "AvailableRoutes" action, and it will limit the deepest route possible.

### Running Tests ###

* You'll need VS2013, at least, Professional.
* Anywhere in visual studio (with all tabs closed), press the following command : ctrl+r, a