﻿using ACLannister.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACLannister.Core.Commands
{
    public class ShortestRouteCommand : BaseCommand
    {
        public override string Execute(CommandParams cmdInfo)
        {
            string minRoute;
            uint minDistance;
            GetShortestRoute(cmdInfo, out minRoute, out minDistance);

            return BuildResult(sb =>
            {
                sb.AppendFormat("- The shortest route between [{0}] is [{1}] with the distance of [{2}]."
                        , cmdInfo.Path, minRoute, minDistance);
            });
        }

        public static void GetShortestRoute(CommandParams cmdInfo
            , out string minRoute, out uint minDistance)
        {
            minRoute = string.Empty;
            minDistance = uint.MaxValue;

            if (cmdInfo.StopCount < 1)
            {
                cmdInfo = cmdInfo.SetStopCount(new Graph(cmdInfo.Graph).Size);
            }
            List<string> allRoutes = AvailableRoutesCommand.GetAllRoutes(cmdInfo);
            foreach (var route in allRoutes)
            {
                var distance = DistanceCommand.CalculateDistance(cmdInfo.SetPath(route));
                if (minDistance > distance)
                {
                    minRoute = route;
                    minDistance = distance;
                }
            }
        }
    }
}
