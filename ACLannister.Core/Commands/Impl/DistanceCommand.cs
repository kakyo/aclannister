﻿using ACLannister.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACLannister.Core.Commands
{
    public class DistanceCommand : BaseCommand
    {
        public override string Execute(CommandParams cmdInfo)
        {
            return BuildResult(sb =>
            {
                sb.AppendFormat("- The distance in [{0}] is [{1}]."
                    , cmdInfo.Path, CalculateDistance(cmdInfo));
            });
        }

        public static uint CalculateDistance(CommandParams cmdInfo)
        {
            Graph graph = new Graph(cmdInfo.Graph);
            uint totalDistance = 0;
            Utils.WalkBetweenCities(cmdInfo.Path, (origin, destination) =>
            {
                totalDistance += graph.GetRouteDistance(origin, destination);
            });
            return totalDistance;
        }
    }
}
