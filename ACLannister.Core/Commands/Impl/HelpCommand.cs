﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACLannister.Core.Commands
{
    public class HelpCommand : BaseCommand
    {
        public override string Execute(CommandParams cmdInfo)
        {
            switch (cmdInfo.Path.ToUpper())
            {
                case  Factory.ACTION_DISTANCE:
                    return WriteHelpForDistance();

                case Factory.ACTION_ALL_ROUTES:
                    return WriteHelpForAvailableRoutes();

                case Factory.ACTION_SHORT_ROUTE:
                    return WriteHelpForShortestRoute();

                default:
                    return WriteHelpForActionNotFound();

            }
        }

        private string WriteHelpForActionNotFound()
        {
            return BuildResult(sb =>
            {
                sb.AppendLine("Use the following format:");
                sb.AppendLine("\thelp|? <action>\n");
                sb.AppendLine("Available actions:");
                sb.AppendLine("\tDistance");
                sb.AppendLine("\tAvailabeRoutes");
                sb.AppendLine("\tShortestRoute");
                sb.AppendLine("\t(?)Help");
            });
        }

        private string WriteHelpForShortestRoute()
        {
            return BuildResult(sb =>
            {
                sb.AppendLine("The \"ShortestRoute\" function will calculate the fastest possible routes between 2 given cities");
                sb.AppendLine("Use the following format:\tShortestRoute|s <path>");
                sb.AppendLine("\t<path>: it's a pair of cities (origin and destination) connected by a hyphen (\"-\")");
                sb.AppendLine("Eg.: ShortestRoute X-Y");
            });
        }

        private string WriteHelpForAvailableRoutes()
        {
            return BuildResult(sb =>
            {
                sb.AppendLine("The \"AvailableRoutes\" function will calculate all possible routes between 2 given cities, with a maximum number of cities between them.");
                sb.AppendLine("Use the following format:\tAvailableRoutes|a <path> <maxStops>");
                sb.AppendLine("\t<path>: it's a pair of cities (origin and destination) connected by a hyphen (\"-\")");
                sb.AppendLine("\t<maxStops>: It's the limit of cities between the origin and destination");
                sb.AppendLine("Eg.: AvailableRoutes X-Y 3");
            });
        }

        private string WriteHelpForDistance()
        {
            return BuildResult(sb =>
            {
                sb.AppendLine("The \"Distance\" function will calculate the total distance in the given path");
                sb.AppendLine("Use the following format:\tDistance|d <path>");
                sb.AppendLine("\t<path>: It's a collection of cities (Letters) grouped by a hyphen (\"-\")");
                sb.AppendLine("Eg.: Distance X-Y");
            });
        }
    }
}
