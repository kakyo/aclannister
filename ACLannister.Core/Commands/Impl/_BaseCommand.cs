﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACLannister.Core.Commands
{
    public abstract class BaseCommand : ICommand
    {
        public abstract string Execute(CommandParams cmdInfo);

        protected string BuildResult(Action<StringBuilder> builder)
        {
            StringBuilder sb = new StringBuilder();
            builder(sb);
            return sb.ToString();
        }
    }
}
