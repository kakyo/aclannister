﻿using ACLannister.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACLannister.Core.Commands
{
    public class AvailableRoutesCommand : BaseCommand
    {
        public override string Execute(CommandParams cmdInfo)
        {
            List<string> allRoutes = GetAllRoutes(cmdInfo);

            return BuildResult(sb =>
            {
                sb.AppendFormat("- The available routes between [{0}] are :", cmdInfo.Path);
                for (var i = 0; i < allRoutes.Count;i++ )
                {
                    sb.AppendFormat(" {0}.[ {1} ]", i+1, allRoutes[i]);
                }
            });
        }

        public static List<string> GetAllRoutes(CommandParams cmdInfo)
        {
            ValidateInput(cmdInfo);
            Graph graph = new Graph(cmdInfo.Graph);

            List<string> allRoutes = new List<string>();
            Utils.WalkBetweenCities(cmdInfo.Path, (origin, destination) =>
            {
                allRoutes = graph.GetRoutes(origin, destination, cmdInfo.StopCount)
                    .Select(s => Utils.BuildPathNotation(origin, s))
                    .ToList();
            });
            //  the list is ordered...
            return allRoutes
                //  by the size of the route, then...
                .OrderBy(o => o.Length)
                //  by the name of the city;
                .ThenBy(o => o)
                .ToList();
        }

        private static void ValidateInput(CommandParams cmdInfo)
        {
            if (cmdInfo.StopCount < 1)
            {
                throw new InvalidNotationException("At least one stop should be selected");
            }
            if (!Utils.ValidateCityPairNotation(cmdInfo.Path))
            {
                throw new InvalidNotationException("The path is incorrect. Please provide only a pair of cities ( X-Y ).");
            }
        }
    }
}
