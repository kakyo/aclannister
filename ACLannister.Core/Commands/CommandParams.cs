﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACLannister.Core
{
    public struct CommandParams
    {
        public string Graph;
        public string Action;
        public string Path;
        public int StopCount;

        public CommandParams(string graph, string action, string path = "", int stopCount = 0)
        {
            Graph = graph;
            Action = action.ToUpper();
            Path = path;
            StopCount = stopCount;
        }

        internal CommandParams(List<string> attributes)
        {
            Graph = attributes[0];
            Action = attributes[1].ToUpper();
            Path = attributes.Count < 3 ? string.Empty : attributes[2];
            if (attributes.Count < 4 || string.IsNullOrEmpty(attributes[3]))
            {
                StopCount = 0;
            }
            else
            {
                StopCount = int.Parse(attributes[3]);
            }
        }

        internal CommandParams SetPath(string path)
        {
            Path = path;
            return this;
        }

        internal CommandParams SetStopCount(int stopCount)
        {
            StopCount = stopCount;
            return this;
        }
    }
}
