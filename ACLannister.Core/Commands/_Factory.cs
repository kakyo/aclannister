﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACLannister.Core.Commands
{
    public static class Factory
    {
        public const string ACTION_DISTANCE = "DISTANCE";
        public const string ACTION_ALL_ROUTES = "AVAILABLEROUTES";
        public const string ACTION_SHORT_ROUTE = "SHORTESTROUTE";
        public const string ACTION_HELP = "HELP";


        static Dictionary<string, ICommand> CommandList;
        static Factory()
        {
            CommandList = new Dictionary<string, ICommand>();
            CommandList.Add(ACTION_DISTANCE, new DistanceCommand());
            CommandList.Add(ACTION_ALL_ROUTES, new AvailableRoutesCommand());
            CommandList.Add(ACTION_SHORT_ROUTE, new ShortestRouteCommand());
            CommandList.Add(ACTION_HELP, new HelpCommand());
        }

        public static string Execute(params string[] args)
        {
            CommandParams cmd = new CommandParams(ParseInput(args));

            if (!CommandList.ContainsKey(cmd.Action))
            {
                return CommandList[ACTION_HELP]
                    .Execute(cmd);
            }

            return CommandList[cmd.Action]
                .Execute(cmd);

        }

        private static List<string> ParseInput(string[] args)
        {
            List<string> argList = new List<string>(4);
            if (args[0] == "?" || args[0].ToUpper() == ACTION_HELP)
            {
                argList.Add(string.Empty);
                if (args.Length < 1)
                {
                    argList.Add(ACTION_HELP);
                }
                else
                {
                    argList.AddRange(args);
                }
            }
            else
            {
                argList = args.ToList();
            }
            return argList;
        }
    }
}
