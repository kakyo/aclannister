﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACLannister.Core
{
    public class UnavailableCityException : Exception
    {
        public UnavailableCityException(string message)
            : base(message) { }

        public UnavailableCityException(string format, params char[] args)
            : base(string.Format(format, args)) { }

        public UnavailableCityException(string format, params string[] args)
            : base(string.Format(format, args)) { }

        public UnavailableCityException(string message, Exception innerException)
            : base(message, innerException) { }
    }
}
