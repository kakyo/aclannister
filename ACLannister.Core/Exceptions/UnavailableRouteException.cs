﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACLannister.Core
{
    public class UnavailableRouteException : Exception
    {
        public UnavailableRouteException(string format, params char[] args)
            : base(string.Format(format, args)) { }

        public UnavailableRouteException(string format, params string[] args)
            : base(string.Format(format, args)) { }

        public UnavailableRouteException(string message, Exception innerException)
            : base(message, innerException) { }
    }
}
