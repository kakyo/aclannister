﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACLannister.Core
{
    public class InvalidNotationException : Exception
    {
        public InvalidNotationException(string message)
            : base(message) { }

        public InvalidNotationException(string format, params char[] args)
            : base(string.Format(format, args)) { }

        public InvalidNotationException(string format, params string[] args)
            : base(string.Format(format, args)) { }

        public InvalidNotationException(string message, Exception innerException)
            : base(message, innerException) { }
    }
}
