﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACLannister.Core.Entities
{
    public class City
    {
        public char Name { get; private set; }
        public List<Route> Routes { get; private set; }

        public City(char name)
        {
            this.Name = name;
            this.Routes = new List<Route>();
        }

        public bool HasRoute(char destination)
        {
            return this.Routes.Any(a => a.Destination.Name == destination);
        }

        public void AddRoute(City destination, uint distance)
        {
            if (!HasRoute(destination.Name))
            {
                this.Routes.Add(new Route(destination, distance));
            }
        }

        public Route FindRoute(char destination)
        {
            if (!HasRoute(destination))
            {
                throw new UnavailableRouteException
                    ("No route between '{0}' and '{1}'.", this.Name.ToString(), destination.ToString());
            }
            return Routes.Single(s => s.Destination.Name == destination);
        }
        
        public uint GetRouteDistance(char destination)
        {
            return FindRoute(destination).Distance;
        }
    }
}
