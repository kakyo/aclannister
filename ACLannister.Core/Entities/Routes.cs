﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACLannister.Core.Entities
{
    public class Route
    {
        public City Destination { get; set; }
        public uint Distance { get; set; }

        public Route(City destination, uint distance)
        {
            this.Destination = destination;
            this.Distance = distance;
        }
    }
}
