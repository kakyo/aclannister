﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ACLannister.Core.Entities
{
    public class Graph
    {
        private const char GRAPH_SEPARATOR = ',';
        private Dictionary<char, City> CityGraph = new Dictionary<char, City>();

        public int Size
        {
            get { return CityGraph.Count; }
        }

        public Graph(string graphNotation)
        {
            string[] nodes = GetGraphNodes(graphNotation);
            foreach (string node in nodes)
            {
                GraphNode graphNode = new GraphNode(node);
                ValidateCity(graphNode.Origin);
                ValidateCity(graphNode.Destination);
                // add route from origin to destination with set distance
                CityGraph[graphNode.Origin].AddRoute(CityGraph[graphNode.Destination], graphNode.Distance);
            }
        }
        
        #region [ Private ]

        private string[] GetGraphNodes(string graphNotation)
        {
            if (Regex.IsMatch(graphNotation, @"\w+\.\w{3}"))
            {
                graphNotation = GetGraphNodesFromFile(graphNotation);
            }

            if (string.IsNullOrEmpty(graphNotation))
            {
                throw new InvalidNotationException("Invalid graph notation");
            }

            return graphNotation.Split(GRAPH_SEPARATOR);
        }

        private string GetGraphNodesFromFile(string graphNotation)
        {
            var assemblyPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            //  if the graph notation come as absolute value
            //  Path.Combine will ignore the "assemblyPath";
            var path = Path.Combine(assemblyPath, graphNotation);

            if (!File.Exists(path))
            {
                throw new FileNotFoundException("The file not found in the following location '" + path + "'");
            }

            return File.ReadAllLines(path).FirstOrDefault();
        }

        private void ValidateCity(char cityName)
        {
            if (!CityGraph.ContainsKey(cityName))
            {
                CityGraph.Add(cityName, new Entities.City(cityName));
            }
        }

        private List<string> GetChildrenRoutes(char origin, char destination, int stopCount)
        {
            Dictionary<char, List<string>> routes
                = new Dictionary<char, List<string>>(CityGraph[origin].Routes.Count);

            foreach (var route in CityGraph[origin].Routes)
            {
                char newOrigin = route.Destination.Name;
                routes.Add(newOrigin, GetRoutes(newOrigin, destination, stopCount - 1));
            }

            return routes
                .Where(w => w.Value != null)
                .SelectMany(s => s.Value.Select(t => Utils.BuildPathNotation(s.Key, t)))
                .ToList();
        }

        #endregion [ Private ]
        #region [ Public ]

        public bool ContainsCity(char p)
        {
            return CityGraph.ContainsKey(p);
        }

        public uint GetRouteDistance(char origin, char destination)
        {
            if (!ContainsCity(origin))
            {
                throw new UnavailableCityException("The city '" + origin + "' do not exist");
            }

            if (!ContainsCity(destination))
            {
                throw new UnavailableCityException("The city '" + destination + "' do not exist");
            }

            return CityGraph[origin].GetRouteDistance(destination);
        }

        public List<string> GetRoutes(char origin, char destination, int stopCount)
        {
            if (stopCount == 0)
            {
                return null;
            }

            List<string> childrenRoutes = GetChildrenRoutes(origin, destination, stopCount);

            if (CityGraph[origin].HasRoute(destination))
            {
                childrenRoutes.Add(destination.ToString());
            }

            return childrenRoutes;
        }

        #endregion [ Public Methods ]
    }
}
