﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ACLannister.Core.Entities
{
    internal struct GraphNode
    {
        public char Origin;
        public char Destination;
        public uint Distance;

        public GraphNode(string nodeNotation)
        {
            nodeNotation = nodeNotation.ToUpper().Trim();
            if (!Regex.IsMatch(nodeNotation, @"\w\w\d+"))
                throw new InvalidNotationException("Invalid node notation");

            uint dist;
            if (!uint.TryParse(nodeNotation.Substring(2), out dist))
            {
                throw new InvalidNotationException("Invalid distance value notation");
            }

            Origin = nodeNotation[0];
            Destination = nodeNotation[1];
            Distance = dist;
        }
    }
}
