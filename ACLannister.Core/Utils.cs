﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ACLannister.Core
{
    internal class Utils
    {
        public static char[] ParsePathNotation(string pathNotation)
        {
            if (!Regex.IsMatch(pathNotation, @"^[a-zA-Z](-?[a-zA-Z])+$"))
            {
                throw new InvalidNotationException("Invalid path notation.");
            }

            return pathNotation.ToUpper().Replace("-", "").ToCharArray();
        }

        public static string BuildPathNotation(char origin, string s)
        {
            return string.Format("{0}-{1}", origin, s);
        }

        public static void WalkBetweenCities(string pathNotation, Action<char, char> walkAction)
        {
            char[] cities = Utils.ParsePathNotation(pathNotation);
            for (var c = 0; c < cities.Length - 1; c++)
            {
                walkAction(cities[c], cities[c + 1]);
            }
        }

        public static bool ValidateCityPairNotation(string pathNotation)
        {
            return Regex.IsMatch(pathNotation, @"^[a-zA-Z]-[a-zA-Z]$");
        }
    }
}
