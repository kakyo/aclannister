﻿using ACLannister.Core;
using ACLannister.Core.Commands;
using ACLannister.Core.Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACLannister.Test
{
    [TestClass]
    public class ShortestRouteTestSuit
    {
        private static void GetRoute(string path, bool fromFile
            , out string minRoute, out uint minDistance)
        {
            CommandParams cmdInfo = new CommandParams(
                fromFile ? "graph.txt" : "AB5, BC4, CD8, DC8, DE6, AD5, CE2, EB3, AE7",
                Factory.ACTION_SHORT_ROUTE,
                path);

            ShortestRouteCommand.GetShortestRoute(cmdInfo,out minRoute,out minDistance);
        }

        [TestMethod]
        public void ShortestRoute()
        {
            string minRoute;
            uint minDistance;

            GetRoute("A-C", false, out minRoute, out minDistance);
            Assert.AreEqual("A-B-C", minRoute);
            Assert.AreEqual<uint>(9, minDistance);
        }

        [TestMethod]
        public void ShortestRouteSameCity()
        {
            string minRoute;
            uint minDistance;

            GetRoute("B-B", false, out minRoute, out minDistance);
            Assert.AreEqual("B-C-E-B", minRoute);
            Assert.AreEqual<uint>(9, minDistance);
        }

        [TestMethod]
        public void ShortestRouteFromGraphFile()
        {
            string minRoute;
            uint minDistance;

            GetRoute("A-C", true, out minRoute, out minDistance);
            Assert.AreEqual("A-B-C", minRoute);
            Assert.AreEqual<uint>(9, minDistance);
        }

        [TestMethod]
        public void ShortestRouteSameCityFromGraphFile()
        {
            string minRoute;
            uint minDistance;

            GetRoute("B-B", true, out minRoute, out minDistance);
            Assert.AreEqual("B-C-E-B", minRoute);
            Assert.AreEqual<uint>(9, minDistance);
        }
    }
}
