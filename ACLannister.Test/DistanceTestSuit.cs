﻿using ACLannister.Core;
using ACLannister.Core.Entities;
using ACLannister.Core.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACLannister.Test
{
    [TestClass]
    public class DistanceTestSuit
    {
        private static uint GetDistance(string path, bool fromFile)
        {
            CommandParams cmdInfo = new Core.CommandParams(
                fromFile ? "graph.txt" : "AB5, BC4, CD80, DC8, DE60, AD5, CE2, EB3, AE7",
                Factory.ACTION_DISTANCE,
                path);
            return DistanceCommand.CalculateDistance(cmdInfo);
        }

        #region [ From File ]

        [TestMethod]
        public void CalculateDistanceFromFile()
        {
            Assert.AreEqual<uint>(9, GetDistance("A-B-C", true));
            // It should allow case-insensitive;
            Assert.AreEqual<uint>(9, GetDistance("abc", true));
        }

        [TestMethod]
        public void CalculateDistanceBigValuesFromFile()
        {
            Assert.AreEqual<uint>(140, GetDistance("C-D-E", true));
        }

        [TestMethod]
        [ExpectedException(typeof(UnavailableCityException))]
        public void CalculateDistanceCityUnavailableFromFile()
        {
            GetDistance("A-B-Z", true);
        }

        [TestMethod]
        [ExpectedException(typeof(UnavailableRouteException))]
        public void CalculateDistanceRouteUnavailableFromFile()
        {
            GetDistance("A-B-A", true);
        }


        [TestMethod]
        [ExpectedException(typeof(InvalidNotationException))]
        public void CalculateDistanceWithWrongPathNotationFromFile()
        {
            GetDistance("A-B-2", true);
        }

        #endregion [ From File ]
        #region [ From Graph Notation ]

        [TestMethod]
        public void CalculateDistance()
        {
            Assert.AreEqual<uint>(9, GetDistance("A-B-C", false));
            // It should allow case-insensitive;
            Assert.AreEqual<uint>(9, GetDistance("abc", false));
        }

        [TestMethod]
        public void CalculateDistanceBigValues()
        {
            Assert.AreEqual<uint>(140, GetDistance("C-D-E", false));
        }

        [TestMethod]
        [ExpectedException(typeof(UnavailableCityException))]
        public void CalculateDistanceCityUnavailable()
        {
            GetDistance("A-B-Z",false);
        }

        [TestMethod]
        [ExpectedException(typeof(UnavailableRouteException))]
        public void CalculateDistanceRouteUnavailable()
        {
            GetDistance("A-B-A",false);
        }


        [TestMethod]
        [ExpectedException(typeof(InvalidNotationException))]
        public void CalculateDistanceWithWrongPathNotation()
        {
            GetDistance("A-B-2", false);
        }

        #endregion [ From Graph Notation ]
    }
}
