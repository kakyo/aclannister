﻿using ACLannister.Core;
using ACLannister.Core.Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ACLannister.Core.Commands;

namespace ACLannister.Test
{
    [TestClass]
    public class AvailableRoutesTestSuit
    {
        private static List<string> GetRoutes(string path, int stopCount, bool fromFile)
        {
            CommandParams cmdInfo = new Core.CommandParams(
                fromFile ? "graph.txt" : "AB5, BC4, CD8, DC8, DE6, AD5, CE2, EB3, AE7",
                Factory.ACTION_ALL_ROUTES,
                path,
                stopCount);

            return AvailableRoutesCommand.GetAllRoutes(cmdInfo);
        }

        #region [ From File ]

        [TestMethod]
        public void AvailableRoutesFromFile()
        {
            List<string> routes = GetRoutes("C-C", 3, true);

            Assert.AreEqual(2, routes.Count);
            Assert.AreEqual("C-D-C", routes[0]);
            Assert.AreEqual("C-E-B-C", routes[1]);
        }

        [TestMethod]
        public void AvailableRoutesBiggerRouteFromFile()
        {
            // It should allow case-insensitive
            List<string> routes = GetRoutes("a-c", 4, true);

            Assert.AreEqual(6, routes.Count);
            Assert.AreEqual("A-B-C", routes[0]);
            Assert.AreEqual("A-D-C", routes[1]);
            Assert.AreEqual("A-E-B-C", routes[2]);
            Assert.AreEqual("A-B-C-D-C", routes[3]);
            Assert.AreEqual("A-D-C-D-C", routes[4]);
            Assert.AreEqual("A-D-E-B-C", routes[5]);
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidNotationException))]
        public void AvailableRoutesInvalidPathNotationFromFile()
        {
            GetRoutes("C-2", 3, true);
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidNotationException))]
        public void AvailableRoutesNoCityPairPathNotationFromFile()
        {
            GetRoutes("A-B-C", 3, true);
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidNotationException))]
        public void AvailableRoutesInvalidStopCountFromFile()
        {
            GetRoutes("C-C", 0, true);
        }

        #endregion [ From File ]
        #region [ From GraphNotation ]

        [TestMethod]
        public void AvailableRoutes()
        {
            List<string> routes = GetRoutes("C-C", 3, false);

            Assert.AreEqual(2, routes.Count);
            Assert.AreEqual("C-D-C", routes[0]);
            Assert.AreEqual("C-E-B-C", routes[1]);
        }
        
        [TestMethod]
        public void AvailableRoutesBiggerRoute()
        {
            // It should allow case-insensitive
            List<string> routes = GetRoutes("a-c", 4, false);

            Assert.AreEqual(6, routes.Count);
            Assert.AreEqual("A-B-C", routes[0]);
            Assert.AreEqual("A-D-C", routes[1]);
            Assert.AreEqual("A-E-B-C", routes[2]);
            Assert.AreEqual("A-B-C-D-C", routes[3]);
            Assert.AreEqual("A-D-C-D-C", routes[4]);
            Assert.AreEqual("A-D-E-B-C", routes[5]);
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidNotationException))]
        public void AvailableRoutesInvalidPathNotation()
        {
            GetRoutes("C-2", 3, false);
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidNotationException))]
        public void AvailableRoutesNoCityPairPathNotation()
        {
            GetRoutes("A-B-C", 3, false);
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidNotationException))]
        public void AvailableRoutesInvalidStopCount()
        {
            GetRoutes("C-C", 0, false);
        }
        
        #endregion [ From GraphNotation ]
    }
}
