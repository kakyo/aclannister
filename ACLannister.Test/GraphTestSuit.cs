﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ACLannister.Core;
using ACLannister.Core.Entities;
using System.IO;

namespace ACLannister.Test
{
    [TestClass]
    public class GraphTestSuit
    {
        [TestMethod]
        public void GenerateGraph()
        {
            Graph graph = new Graph("AB5,bc6");
            Assert.IsNotNull(graph);
            Assert.IsTrue(graph.ContainsCity('B'));
            //case-insensitive for graph notation
            Assert.IsFalse(graph.ContainsCity('b'));
        }

        [TestMethod]
        public void GenerateGraphFromRelativePath()
        {
            Graph graph = new Graph("graph.txt");
            Assert.IsNotNull(graph);
            Assert.IsTrue(graph.ContainsCity('B'));
        }

        [TestMethod]
        public void GenerateGraphFromAbsolutePath()
        {
            Graph graph = new Graph(@"C:\Users\LuísAlberto\Desktop\graph.txt");
            Assert.IsNotNull(graph);
            Assert.IsTrue(graph.ContainsCity('B'));
        }

        [TestMethod]
        [ExpectedException(typeof(FileNotFoundException))]
        public void GenerateGraphFromUnavailableFile()
        {
            Graph graph = new Graph("jasdkfjasf.txt");
        }

        [TestMethod]
        [ExpectedException(typeof(FileNotFoundException))]
        public void GenerateGraphFromUnavailableFileAbsolutePath()
        {
            Graph graph = new Graph(@"C:\jasdkfjasf.txt");
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidNotationException))]
        public void GenerateGraphWithEmptyData()
        {
            Graph graph = new Graph(string.Empty);
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidNotationException))]
        public void GenerateGraphWithEmptyDataInFile()
        {
            Graph graph = new Graph("graph_empty.txt");
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidNotationException))]
        public void GenerateGraphWithInvalidData()
        {
            Graph graph = new Graph("kjhkdfahsdkjasf");
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidNotationException))]
        public void GenerateGraphWithInvalidDataInFile()
        {
            Graph graph = new Graph("graph_invalid.txt");
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidNotationException))]
        public void GenerateGraphWithWrongNotation()
        {
            Graph graph = new Graph("AB5; BC4, CD8");
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidNotationException))]
        public void GenerateGraphWithWrongNotationInFile()
        {
            Graph graph = new Graph("graph_wrongGraph.txt");
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidNotationException))]
        public void GenerateGraphWithWrongNodeNotation()
        {
            Graph graph = new Graph("AB, BC4, CD8");
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidNotationException))]
        public void GenerateGraphWithWrongNodeNotationInFile()
        {
            Graph graph = new Graph("graph_wrongNode.txt");
        }
    }
}
