﻿using ACLannister.Core.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace ACLannister.Restful
{
    public class Service : IService
    {
        private string RunAction(string action, string graph, string path, string stopCount = "")
        {
            try
            {
                string[] args = new string[4]{
                    graph,
                    action,
                    path,
                    stopCount
                };
                return Factory.Execute(args);
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public string DistanceXml(string graph, string path)
        {
            return RunAction(Factory.ACTION_DISTANCE, graph, path);
        }

        public string DistanceJson(string graph, string path)
        {
            return RunAction(Factory.ACTION_DISTANCE, graph, path);
        }

        public string AvailableXml(string graph, string path, string stopCount)
        {
            return RunAction(Factory.ACTION_ALL_ROUTES, graph, path, stopCount);
        }

        public string AvailableJson(string graph, string path, string stopCount)
        {
            return RunAction(Factory.ACTION_ALL_ROUTES, graph, path, stopCount);
        }

        public string ShortestXml(string graph, string path)
        {
            return RunAction(Factory.ACTION_SHORT_ROUTE, graph, path);
        }

        public string ShortestJson(string graph, string path)
        {
            return RunAction(Factory.ACTION_SHORT_ROUTE, graph, path);
        }
    }
}
