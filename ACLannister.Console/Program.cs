﻿using ACLannister.Core;
using ACLannister.Core.Entities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACLannister.Console
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                Log(Core.Commands.Factory.Execute(args));
            }
            catch (Exception exp)
            {
                Log("** ERROR **\n{0}", exp.Message);
            }
        }

        private static void Log(string format, params object[] args)
        {
            System.Console.WriteLine(format, args);
        }
    }
}
